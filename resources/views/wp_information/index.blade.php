@extends('app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>WP Control Panel List</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>WP Control Panel List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>WP Control Panel List</small></h5>
                            <div class="ibox-tools">

                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>

                            </div>
                        </div>
                        <div class="ibox-content">
                            <table id="WcpList" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>WP Name</th>
                                        <th>Email</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('wp-information')
<script>
$(document).ready(function(){
     $(function() {

        $('#WcpList').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'wcp-list',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'wp_name', name: 'wp_name'},
            {data: 'email', name: 'email'},
            {data: 'edit', name: 'edit', orderable: false, searchable: false}
        ]
    });
    });
}); 
</script>           
@endsection
