@extends('app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit WP Information</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>Edit WP Information</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit WP Information</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                               
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                  @if(Session::has('alert-' . $msg))
                                  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                                  @endif
                                @endforeach
                            </div>
                            {!! Form::model($wp_information, array('route' => array('wp_information.update', $wp_information->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}    
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group"><label class="col-sm-2 control-label">WP Name</label>
                                    <div class="col-sm-10"><input type="text" name="wp_name" id="wp_name" class="form-control" value="{{$wp_information->wp_name}}" readonly="readonly"></div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10"><input type="text" name="description" id="description" class="form-control" value="{{$wp_information->description}}"></div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10"><input type="text" name="email" id="email" class="form-control" value="{{$wp_information->email}}"></div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        
                                         {!! Form::select('status', [
                                        '' => 'Choose One', 
                                        '1' => 'Active', 
                                        '0' => 'In-Active',
                                        ],
                                         $wp_information->status, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                        
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
