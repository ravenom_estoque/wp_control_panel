<style>
/* 
Generic Styling, for Desktops/Laptops 
*/
table { 
  width: 100%; 
  border-collapse: collapse; 
}
/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
}
th { 
  background: #333; 
  color: white; 
  font-weight: bold; 
}
td, th { 
  padding: 6px; 
  border: 1px solid #ccc; 
  text-align: left; 
}
</style>

<h3>Hello QC Team,</h3>

<p>We need the recordings of the listed phone numbers below to be uploaded to the ftp site within 24-72 hours please.</p>
<p>Kindly notify us as soon as these are uploaded to the ftp site. </p>

<table>
	<thead>
		<th>#</th>
		<th>Title</th>
		<th>Firstname</th>
		<th>Lastname</th>
		<th>Telephone</th>
		<th>Filename</th>
	</thead>
	{!!$body!!}
</table>

