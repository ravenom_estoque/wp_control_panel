<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Models\Wcp;
use Validator;
use Input;
use Redirect;
use Session;
use View;
use Auth;
use Datatables;
use DB;
use Mail;

class Wp_control_panelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wp_information.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('wp_information.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
          'wp_name'        => 'required',
          'email'          => 'required',  
        );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails())
      {
          return Redirect::to('wp_information/create')->withErrors($validator);
      }
      else
      {
          $wcp = new Wcp();
          $wcp->wp_name             = Input::get("wp_name");
          $wcp->email               = Input::get("email");
          $wcp->description         = Input::get("description");
         
          if($wcp->save())
          {
              Session::flash('alert-success', 'Form Submitted Successfully.');
          }
          
          else
          {
              Session::flash('alert-danger', 'Form Submitted Successfully.');
          }
          return Redirect::to('wp_information/create');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wp_information = Wcp::find($id);
        return view('wp_information.edit')->with('wp_information', $wp_information);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
          'wp_name'        => 'required',
          'email'          => 'required',  
          'status'         => 'required',  
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::to('wp_information/'.$id.'/edit')->withErrors($validator);
        }
        else
        {
          $wp = Wcp::find($id);
          $wp->email = $request->email;
          $wp->status = $request->status;
          $wp->description = $request->description;
          $wp->save();

          Session::flash('alert-success', 'WP Successfully Updated.');
          return Redirect::to('wp_information/'.$id.'/edit');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getWcpList()
    {
      $wcp = Wcp::select(['id','wp_name', 'email'])->where('wp_name', '!=', 'MIS');
      return Datatables::of($wcp)
            ->addColumn('edit', function ($wcp) {
                return '<a href="wp_information/'.$wcp->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }

    public function generateEmail()
    {
        // Query all WP's and their status active (1)
        $wp = Wcp::where('status', '=', 1)->where('wp_name', '!=', 'MIS')->get();
       
        foreach ($wp as $key => $value) 
        { // Loop each WP
          $data_email = array();
          $query = "select distinct(columnheadername) from $value->wp_name.dbo.Deliveries where right(filename,8) between convert(nvarchar(8),dateadd(dd,-9,getdate()),112) and convert(nvarchar(8),dateadd(dd,-3,getdate()),112)";
          $campaigns = DB::connection('sqlsrv')->select($query);  // Get all campaigns for each WP

          foreach ($campaigns as $key => $value2) 
          { // Get how many records per campaign
            $number_get = 0;
            $query2 = "select count(*) as cnt from $value->wp_name.dbo.Deliveries where right(filename,8) between convert(nvarchar(8),dateadd(dd,-9,getdate()),112) and convert(nvarchar(8),dateadd(dd,-3,getdate()),112) and ColumnHeaderName ='$value2->columnheadername'";
            $data_count = DB::connection('sqlsrv')->select($query2); 

            $count = intval($data_count[0]->cnt);
            $whole = floor($count/100);
            $remainder = floor($count%100);

            $number_get += $whole;
            if($remainder == $count && $whole == 0) {$number_get += 1;}
            if($remainder > 0 && $remainder != $count) {$number_get += 1;}

            $query3  = "select TOP $number_get Title, Firstname, Surname, Telephone, Filename from $value->wp_name.dbo.Deliveries where right(filename,8) between convert(nvarchar(8),dateadd(dd,-9,getdate()),112) and convert(nvarchar(8),dateadd(dd,-3,getdate()),112) and ColumnHeaderName ='$value2->columnheadername' order by newid()";
            $wp_data = DB::connection('sqlsrv')->select($query3); 

            $data_arr = array('WP' => $value->wp_name, 'Header' => $value2->columnheadername, 'Data' => $wp_data);
            array_push($data_email, $data_arr);
            
          }

          // Send email in the WP 
          $get_wp_email = Wcp::where('wp_name', '=', $value->wp_name)->first();
          $body = '';
          $emails = explode(";", $get_wp_email->email); // Get all wp email addresses
          $counter = 1;

          foreach ($data_email as $key => $value3) {
            foreach ($value3['Data'] as $key1 => $value4) {
              $body .= "<tr>" .
              "<td>".$counter."</td>".
              "<td>".$value4->Title."</td>".
              "<td>".$value4->Firstname."</td>".
              "<td>".$value4->Surname."</td>".
              "<td>".$value4->Telephone."</td>".
              "<td>".$value4->Filename."</td>".
              "</tr>";
              $counter++;
            }
          }
          
          $wp_name = $value->wp_name;

          $send = Mail::send('emails.message', 
            [
            'body' => $body,
            'name' => '',
            'date' => date('l jS \of F Y h:i:s A')

            ], function ($m) use ($emails, $wp_name) {
            $m->to($emails, '')->subject('Recording Request for '.$wp_name); 
            $m->from('quality@qdf-phils.com', $name = null);     
            $m->cc('mis@qdf-phils.com', 'MIS');  
          });

          
        }


    }
}